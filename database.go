package lime

import (
	"gorm.io/gorm"
)

func DatabaseProvider(models DatabaseModels, config *DatabaseConfiguration) *gorm.DB {
	starter := DatabaseStarter{
		log:           NewLogger("DatabaseStarter"),
		configuration: config,
		models:        models,
	}
	return starter.Start()
}
