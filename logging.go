package lime

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/diode"
	"os"
	"time"
)

type LogLevel zerolog.Level

func (l LogLevel) Level() zerolog.Level {
	return zerolog.Level(l)
}

func LogLevelBy(level string) LogLevel {
	switch level {
	case "DEBUG", "Debug":
		return DebugLevel
	case "INFO", "Info":
		return InfoLevel
	case "WARN", "Warn":
		return WarnLevel
	case "ERROR", "Error":
		return ErrorLevel
	case "FATAL", "Fatal":
		return FatalLevel
	case "PANIC", "Panic":
		return PanicLevel
	case "NO", "No":
		return NoLevel
	case "DISABLED", "Disabled":
		return Disabled
	case "TRACE", "Trace":
		return TraceLevel
	}
	return NoLevel
}

const (
	DebugLevel = LogLevel(zerolog.DebugLevel)
	InfoLevel  = LogLevel(zerolog.InfoLevel)
	WarnLevel  = LogLevel(zerolog.WarnLevel)
	ErrorLevel = LogLevel(zerolog.ErrorLevel)
	FatalLevel = LogLevel(zerolog.FatalLevel)
	PanicLevel = LogLevel(zerolog.PanicLevel)
	NoLevel    = LogLevel(zerolog.NoLevel)
	Disabled   = LogLevel(zerolog.Disabled)
	TraceLevel = LogLevel(zerolog.TraceLevel)
)

type LogConfiguration struct {
	GlobalLevel LogLevel
}

type Logger struct {
	logger zerolog.Logger
}

func (l *Logger) Debug() *zerolog.Event {
	return l.logger.Debug()
}

func (l *Logger) Info() *zerolog.Event {
	return l.logger.Info()
}

func (l *Logger) Warn() *zerolog.Event {
	return l.logger.Warn()
}

func (l *Logger) Error() *zerolog.Event {
	return l.logger.Error()
}

func (l *Logger) Fatal() *zerolog.Event {
	return l.logger.Fatal()
}

func (l *Logger) WithLevel(level LogLevel) *zerolog.Event {
	return l.logger.WithLevel(level.Level())
}

func NewLogger(name string) *Logger {
	whenMissed := func(missed int) {
		fmt.Printf("Logger Dropped %d messages", missed)
	}
	// activate non-blocking logging
	wr := diode.NewWriter(os.Stdout, 1000, 100*time.Millisecond, whenMissed)
	asyncOut := zerolog.ConsoleWriter{Out: wr, TimeFormat: time.Stamp}
	asyncLogger := zerolog.New(asyncOut).With().Timestamp().Logger()

	return &Logger{
		logger: asyncLogger.With().Str("name", name).Logger(),
	}
}

type RequestLogWriter struct {
	logger *Logger
	level  LogLevel
}

func (w RequestLogWriter) Write(p []byte) (n int, err error) {
	n = len(p)
	if n > 0 && p[n-1] == '\n' {
		p = p[0 : n-1]
	}
	w.logger.
		WithLevel(w.level).
		Msg(string(p))
	return
}

func DebugRequestWriter(logger *Logger) *RequestLogWriter {
	return &RequestLogWriter{
		logger: logger,
		level:  DebugLevel,
	}
}
