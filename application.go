package lime

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"gitlab.com/hd/lock"
)

type Application struct {
	log       *Logger
	config    *ApplicationConfiguration
	runtime   *RuntimeContext
	stopLock  lock.Lock
	isRunning AtomicBool
}

func (app *Application) configure() {
	// Log configuration
	var engineLogger gin.HandlerFunc
	logging := app.config.logging
	if logging != nil {
		level := zerolog.Level(logging.GlobalLevel)
		zerolog.SetGlobalLevel(level)
		app.log.logger.Level(level)
		gin.DebugPrintRouteFunc = func(httpMethod, absolutePath, handlerName string, nuHandlers int) {
			app.log.Debug().Msgf("%-6s %-25s --> %s (%d handlers)\n", httpMethod, absolutePath, handlerName, nuHandlers)
		}
		engineLogger = gin.LoggerWithWriter(DebugRequestWriter(app.log))
	}

	// DB configuration
	models := app.config.models
	database := app.config.database
	if database != nil && models != nil {
		db := DatabaseProvider(models, database)
		if db != nil {
			app.runtime.Database = db
		}
	}

	// Gin configuration
	engine := createEngine(engineLogger)
	app.runtime.Engine = engine

	// CORS configuration
	config := cors.DefaultConfig()
	if app.config.cors == nil {
		app.config.cors = DefaultCorsConfiguration()
	}
	corsConfig := app.config.cors
	config.AllowOrigins = corsConfig.AllowOrigins
	config.AllowHeaders = corsConfig.AllowHeaders
	config.AllowMethods = corsConfig.AllowMethods
	config.AllowCredentials = corsConfig.AllowCredentials
	engine.Use(cors.New(config))

	// Routing configuration
	routerFactory := app.config.routerFactory
	if routerFactory != nil {
		router := routerFactory(app.runtime)
		if router != nil {
			router.RegisterRoutes(engine)
		}
		addr := ":8080"
		if app.config.port > 0 && app.config.port < 65536 {
			addr = fmt.Sprintf(":%d", app.config.port)
		}
		app.runtime.Server = &http.Server{
			Addr:    addr,
			Handler: engine,
		}
	}
}

func createEngine(engineLogger gin.HandlerFunc) *gin.Engine {
	if engineLogger != nil {
		engine := gin.New()
		engine.Use(engineLogger, gin.Recovery())
		return engine
	}
	return gin.Default()
}

func (app *Application) Start() error {
	go app.doStop()
	app.isRunning.setTrue()
	if err := app.runtime.Server.ListenAndServe(); err != nil {

		app.isRunning.setFalse()

		if err == http.ErrServerClosed {
			return nil
		}

		app.log.Error().Stack().
			Err(err).Msg(err.Error())
		return err
	}
	return nil
}

func (app *Application) IsRunning() bool {
	return app.isRunning.isSet()
}

func (app *Application) Stop() {
	app.isRunning.setFalse()
	app.stopLock.Unlock()
}

func (app *Application) doStop() {
	app.log.Debug().Msg("Waiting interrupt signal...")
	app.stopLock.Wait()

	if app.runtime.Server != nil {
		app.log.Info().Msg("Received interrupt signal")

		app.isRunning.setFalse()
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := app.runtime.Server.Shutdown(ctx); err != nil {
			app.log.Error().Stack().
				Err(err).Msgf("Error stopping Server: %v", err.Error())
		}
	}
}
