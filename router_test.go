package lime

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"testing"
)

type TestController struct {
}

func (c TestController) Register(engine *gin.Engine) {
	engine.GET("", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
}

func NewTestRouter() *Router {
	return &Router{
		Routes: []RoutedController{
			&TestController{},
		},
	}
}

func TestRouter_RegisterRoutes(t *testing.T) {
	engine := gin.Default()
	router := NewTestRouter()

	router.RegisterRoutes(engine)

	assert.Equal(t, 1, len(router.Routes))
	assert.Equal(t, 2, len(engine.RouterGroup.Handlers))
}
