package lime

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewLogger(t *testing.T) {
	logger := NewLogger("test")
	assert.NotEmpty(t, logger)
}

func TestLogger_Debug(t *testing.T) {
	logger := NewLogger("test")
	assert.NotEmpty(t, logger.Debug())
}

func TestLogger_Info(t *testing.T) {
	logger := NewLogger("test")
	assert.NotEmpty(t, logger.Info())
}

func TestLogger_Warn(t *testing.T) {
	logger := NewLogger("test")
	assert.NotEmpty(t, logger.Warn())
}

func TestLogger_Error(t *testing.T) {
	logger := NewLogger("test")
	assert.NotEmpty(t, logger.Error())
}

func TestLogger_Fatal(t *testing.T) {
	logger := NewLogger("test")
	assert.NotEmpty(t, logger.Fatal())
}

func TestLogLevelBy(t *testing.T) {
	var tests = []struct {
		input    string
		expected LogLevel
	}{
		{"", NoLevel},
		{string([]byte{}), NoLevel},
		{"DEBUG", DebugLevel},
		{"Debug", DebugLevel},
		{"INFO", InfoLevel},
		{"Info", InfoLevel},
		{"WARN", WarnLevel},
		{"Warn", WarnLevel},
		{"ERROR", ErrorLevel},
		{"Error", ErrorLevel},
		{"FATAL", FatalLevel},
		{"Fatal", FatalLevel},
		{"PANIC", PanicLevel},
		{"Panic", PanicLevel},
		{"NO", NoLevel},
		{"No", NoLevel},
		{"DISABLED", Disabled},
		{"Disabled", Disabled},
		{"TRACE", TraceLevel},
		{"Trace", TraceLevel},
	}
	for _, test := range tests {
		if result := LogLevelBy(test.input); result != test.expected {
			t.Errorf("LogLevelBy(%q = %v)", test.input, result)
		}
	}
}
