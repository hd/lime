package lime

import (
	"bytes"
	"encoding/json"
	"gitlab.com/hd/lock"
	"net/http"
	"net/http/httptest"
	"testing"
)

type TestContext struct {
	application *Application
}

func AssertPanic(t *testing.T, f func()) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	f()
}

type TestAfterStart func(t *testing.T)

func (ctx *TestContext) RunTest(t *testing.T, postStartCall TestAfterStart) {
	runTestLock := lock.New()
	app := ctx.application
	go func() {
		runTestLock.Unlock()
		if err := app.Start(); err != nil {
			app.log.Error().Stack().
				Err(err).Msg(err.Error())
		}
	}()

	runTestLock.Wait()

	postStartCall(t)

	app.Stop()
}

func (ctx *TestContext) CreateEntity(value interface{}) error {
	app := ctx.application
	db := app.runtime.Database
	if db != nil {
		result := db.Create(value)
		err := result.Error
		if err != nil {
			app.log.Error().Stack().
				Err(err).Msg(err.Error())
			return err
		}
	}
	return nil
}

func (ctx *TestContext) Perform(req *http.Request) *httptest.ResponseRecorder {
	engine := ctx.application.runtime.Engine
	if engine != nil {
		resp := httptest.NewRecorder()
		engine.ServeHTTP(resp, req)
		return resp
	}
	return nil
}

func (ctx *TestContext) GET(uri string) *httptest.ResponseRecorder {
	app := ctx.application
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		app.log.Error().Stack().
			Err(err).Msg(err.Error())
		return nil
	}
	return ctx.Perform(req)
}

func (ctx *TestContext) DELETE(uri string) *httptest.ResponseRecorder {
	app := ctx.application
	req, err := http.NewRequest(http.MethodDelete, uri, nil)
	if err != nil {
		app.log.Error().Stack().
			Err(err).Msg(err.Error())
		return nil
	}
	return ctx.Perform(req)
}

func (ctx *TestContext) POST(uri string, body interface{}) *httptest.ResponseRecorder {
	app := ctx.application
	payload := new(bytes.Buffer)
	err := json.NewEncoder(payload).Encode(body)
	if err != nil {
		app.log.Error().Stack().
			Err(err).Msg(err.Error())
		return nil
	}
	req, err := http.NewRequest(http.MethodPost, uri, payload)
	if err != nil {
		app.log.Error().Stack().
			Err(err).Msg(err.Error())
		return nil
	}
	return ctx.Perform(req)
}

func (ctx *TestContext) PUT(uri string, body interface{}) *httptest.ResponseRecorder {
	app := ctx.application
	payload := new(bytes.Buffer)
	err := json.NewEncoder(payload).Encode(body)
	if err != nil {
		app.log.Error().Stack().
			Err(err).Msg(err.Error())
		return nil
	}
	req, err := http.NewRequest(http.MethodPut, uri, payload)
	if err != nil {
		app.log.Error().Stack().
			Err(err).Msg(err.Error())
		return nil
	}
	return ctx.Perform(req)
}
