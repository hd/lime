package lime

import "github.com/gin-contrib/cors"

type CorsConfiguration struct {
	AllowOrigins     []string
	AllowMethods     []string
	AllowHeaders     []string
	AllowCredentials bool
}

func DefaultCorsConfiguration() *CorsConfiguration {
	config := &CorsConfiguration{
		AllowOrigins:     []string{"*"},
		AllowHeaders:     cors.DefaultConfig().AllowHeaders,
		AllowMethods:     cors.DefaultConfig().AllowMethods,
		AllowCredentials: cors.DefaultConfig().AllowCredentials,
	}
	return config
}
