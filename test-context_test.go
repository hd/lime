package lime

import (
	"github.com/gin-gonic/gin"
	"github.com/kinbiko/jsonassert"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"net/http"
	"testing"
)

type Mock struct {
	gorm.Model
	Name string
}

type Dummy struct {
	gorm.Model
	Name string
}

type MockRepository struct {
	db *gorm.DB
}

type MockController struct {
	repository *MockRepository
}

func (mc *MockController) Register(engine *gin.Engine) {
	endpoint := engine.Group("/mock")
	{
		endpoint.GET("", mc.HelloWorld)
	}
}

func (mc *MockController) HelloWorld(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "hello world"})
}

func MockDatabaseModels() DatabaseModels {
	return []DatabaseModel{
		&Mock{},
	}
}

func NewMockDatabaseConfig() *DatabaseConfiguration {
	return &DatabaseConfiguration{
		DSN:     "file::memory:?cache=shared",
		Connect: sqlite.Open,
	}
}

func NewMockRouter(ctx *RuntimeContext) *Router {
	db := ctx.Database
	return &Router{
		Routes: []RoutedController{
			&MockController{&MockRepository{db: db}},
		},
	}
}

func CreateTestContext() *TestContext {
	router := NewMockRouter
	models := MockDatabaseModels()
	database := NewMockDatabaseConfig()
	return TestContextWith().
		Models(models).
		Database(database).
		Router(router).
		Configure()
}

func TestTestContext_Create(t *testing.T) {
	testContext := CreateTestContext()
	assert.NotEmpty(t, testContext)
	assert.NotEmpty(t, testContext.application)
}

func TestTestContext_CreateEntity(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		entity := Mock{
			Name: "test-1",
		}
		err := testContext.CreateEntity(&entity)
		assert.Empty(t, err)
	})
}

func TestTestContext_Perform(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		req, err := http.NewRequest("GET", "/mock", nil)
		assert.Empty(t, err)

		resp := testContext.Perform(req)
		assert.NotNil(t, resp)
		assert.Equal(t, http.StatusOK, resp.Code)
		s := resp.Body.String()
		jsonassert.New(t).Assertf(s,
			`{ "message" : "hello world" }`)
	})
}

func TestTestContext_Perform_Error(t *testing.T) {
	testContext := CreateTestContext()
	testContext.application.runtime.Engine = nil
	testContext.RunTest(t, func(t *testing.T) {
		req, err := http.NewRequest("GET", "/mock", nil)
		assert.Empty(t, err)

		resp := testContext.Perform(req)
		assert.Empty(t, resp)
	})
}

func TestTestContext_GET(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.GET("/mock")
		assert.NotNil(t, resp)
		assert.Equal(t, http.StatusOK, resp.Code)
		s := resp.Body.String()
		jsonassert.New(t).Assertf(s,
			`{ "message" : "hello world" }`)
	})
}

func TestTestContext_GET_UrlError(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.GET("\n")
		assert.Nil(t, resp)
	})
}

func TestTestContext_DELETE(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.DELETE("/mock")
		assert.NotNil(t, resp)
		assert.Equal(t, http.StatusNotFound, resp.Code)
	})
}

func TestTestContext_DELETE_UrlError(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.DELETE("\n")
		assert.Nil(t, resp)
	})
}

func TestTestContext_POST(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.POST("/mock", &Mock{})
		assert.NotNil(t, resp)
		assert.Equal(t, http.StatusNotFound, resp.Code)
	})
}

func TestTestContext_POST_UrlError(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.POST("\n", &Mock{})
		assert.Nil(t, resp)
	})
}
func TestTestContext_PUT(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.PUT("/mock", &Mock{})
		assert.NotNil(t, resp)
		assert.Equal(t, http.StatusNotFound, resp.Code)
	})
}

func TestTestContext_PUT_UrlError(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.PUT("\n", &Mock{})
		assert.Nil(t, resp)
	})
}
