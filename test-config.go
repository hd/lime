package lime

type TestConfiguration struct {
	ApplicationConfiguration
}

func (c *TestConfiguration) Model(model DatabaseModel) *TestConfiguration {
	if c.models != nil {
		c.models = append(c.models, model)
	} else {
		c.models = []DatabaseModel{model}
	}
	return c
}

func (c *TestConfiguration) Models(models []DatabaseModel) *TestConfiguration {
	if c.models != nil {
		c.models = append(c.models, models...)
	} else {
		c.models = models
	}
	return c
}

func (c *TestConfiguration) Database(databaseConfig *DatabaseConfiguration) *TestConfiguration {
	c.database = databaseConfig
	return c
}

func (c *TestConfiguration) CorsAllowOrigins(allowOrigins []string) *TestConfiguration {
	c.ApplicationConfiguration.CorsAllowOrigins(allowOrigins)
	return c
}

func (c *TestConfiguration) CorsAllowMethods(allowMethods []string) *TestConfiguration {
	c.ApplicationConfiguration.CorsAllowMethods(allowMethods)
	return c
}

func (c *TestConfiguration) CorsAllowHeaders(allowHeaders []string) *TestConfiguration {
	c.ApplicationConfiguration.CorsAllowHeaders(allowHeaders)
	return c
}

func (c *TestConfiguration) CorsAllowCredentials(allowCredentials bool) *TestConfiguration {
	c.ApplicationConfiguration.CorsAllowCredentials(allowCredentials)
	return c
}

func (c *TestConfiguration) Logging(logConfiguration *LogConfiguration) *TestConfiguration {
	c.logging = logConfiguration
	return c
}

func (c *TestConfiguration) Router(routerFactory RouterFactory) *TestConfiguration {
	c.routerFactory = routerFactory
	return c
}

func (c *TestConfiguration) Port(port int) *TestConfiguration {
	c.port = port
	return c
}

func (c *TestConfiguration) Configure() *TestContext {
	app := c.ApplicationConfiguration.Configure()
	return &TestContext{
		application: app,
	}
}

func TestContextWith() *TestConfiguration {
	logging := &LogConfiguration{
		GlobalLevel: DebugLevel,
	}

	tcc := &TestConfiguration{}
	return tcc.
		Port(8888).
		Logging(logging)
}
