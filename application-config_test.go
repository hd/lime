package lime

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestApplicationConfiguration_ConfigureMinimal(t *testing.T) {
	config := ApplicationWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig())
	assert.NotEmpty(t, config)
	assert.NotEmpty(t, config.database)
	assert.Equal(t, NewMockDatabaseConfig().DSN, config.database.DSN)
	assert.Empty(t, config.logging)
	assert.Empty(t, config.cors)
}

func TestApplicationConfiguration_ConfigureAll(t *testing.T) {
	config := ApplicationWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Logging(&LogConfiguration{GlobalLevel: WarnLevel}).
		CorsAllowOrigins([]string{"*"}).
		CorsAllowMethods([]string{"GET"}).
		CorsAllowHeaders([]string{"X-Test"}).
		Port(8888).
		Router(NewMockRouter)
	assert.NotEmpty(t, config)
	assert.NotEmpty(t, config.database)
	assert.Equal(t, NewMockDatabaseConfig().DSN, config.database.DSN)
	assert.NotEmpty(t, config.logging)
	assert.NotEmpty(t, config.cors)
	assert.Equal(t, []string{"*"}, config.cors.AllowOrigins)
	assert.Equal(t, []string{"X-Test"}, config.cors.AllowHeaders)
	assert.Equal(t, []string{"GET"}, config.cors.AllowMethods)
	assert.Equal(t, false, config.cors.AllowCredentials)
	assert.NotEmpty(t, WarnLevel, config.logging.GlobalLevel)
	assert.NotEmpty(t, config.cors)
	assert.Equal(t, 8888, config.port)
}

func TestApplicationConfiguration_ConfigureAppendModels(t *testing.T) {
	config := ApplicationWith().
		Model(&Mock{}).
		Model(&Dummy{}).
		Database(NewMockDatabaseConfig())
	assert.NotEmpty(t, config)
	assert.NotEmpty(t, config.database)
	assert.Equal(t, NewMockDatabaseConfig().DSN, config.database.DSN)
	assert.Equal(t, 2, len(config.models))
}

func TestApplicationConfiguration_ConfigureAppendModelsAndCombine(t *testing.T) {
	config := ApplicationWith().
		Model(&Dummy{}).
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig())
	assert.NotEmpty(t, config)
	assert.NotEmpty(t, config.database)
	assert.Equal(t, NewMockDatabaseConfig().DSN, config.database.DSN)
	assert.Equal(t, 2, len(config.models))
}

func TestApplicationConfiguration_DefaultCorsConfigIsEmpty(t *testing.T) {
	config := ApplicationWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig())
	assert.NotEmpty(t, config)
	assert.Empty(t, config.cors)
}
