package lime

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTestContextConfiguration_Model(t *testing.T) {
	testContext := TestContextWith().
		Model(&Mock{}).
		Model(&Dummy{}).
		Database(NewMockDatabaseConfig()).
		Configure()
	assert.NotEmpty(t, testContext)
	assert.NotEmpty(t, testContext.application)
	assert.NotEmpty(t, testContext.application.config)
	assert.NotEmpty(t, testContext.application.config.models)
	assert.Equal(t, 2, len(testContext.application.config.models))
}

func TestTestContextConfiguration_Models(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Configure()
	assert.NotEmpty(t, testContext)
	assert.NotEmpty(t, testContext.application)
	assert.NotEmpty(t, testContext.application.config)
	assert.NotEmpty(t, testContext.application.config.models)
	assert.Equal(t, 1, len(testContext.application.config.models))
}

func TestTestContextConfiguration_ModelsCombine(t *testing.T) {
	testContext := TestContextWith().
		Model(&Dummy{}).
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Configure()
	assert.NotEmpty(t, testContext)
	assert.NotEmpty(t, testContext.application)
	assert.NotEmpty(t, testContext.application.config)
	assert.NotEmpty(t, testContext.application.config.models)
	assert.Equal(t, 2, len(testContext.application.config.models))
}

func TestTestContextConfiguration_WithDefaultPort(t *testing.T) {
	testContext := TestContextWith().
		Model(&Mock{}).
		Database(NewMockDatabaseConfig()).
		Configure()
	assert.NotEmpty(t, testContext)
	assert.NotEmpty(t, testContext.application)
	assert.NotEmpty(t, testContext.application.config)
	assert.Equal(t, 8888, testContext.application.config.port)
}

func TestTestContextConfiguration_WithPort(t *testing.T) {
	testContext := TestContextWith().
		Model(&Mock{}).
		Database(NewMockDatabaseConfig()).
		Port(9090).
		Configure()
	assert.NotEmpty(t, testContext)
	assert.NotEmpty(t, testContext.application)
	assert.NotEmpty(t, testContext.application.config)
	assert.Equal(t, 9090, testContext.application.config.port)
}
