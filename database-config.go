package lime

import "gorm.io/gorm"

type connect func(string) gorm.Dialector
type DatabaseModels []DatabaseModel
type DatabaseModel interface{}

type DatabaseConfiguration struct {
	DSN        string
	Connect    connect
	GormConfig *gorm.Config
}
