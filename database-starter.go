package lime

import (
	"gorm.io/gorm"
)

type DatabaseStarter struct {
	models        DatabaseModels
	configuration *DatabaseConfiguration
	log           *Logger
}

func (ds *DatabaseStarter) Start() *gorm.DB {
	db := ds.connect()
	ds.migrate(db)
	return db
}

func (ds *DatabaseStarter) migrate(db *gorm.DB) {
	for _, model := range ds.models {
		err := db.AutoMigrate(model)
		if err != nil {
			panic("failed to migrate table")
		}
	}
	ds.log.Info().
		Msg("DatabaseStarter initialized.")
}

func (ds *DatabaseStarter) connect() *gorm.DB {
	dsn := ds.configuration.DSN
	config := ds.configuration.GormConfig
	if config == nil {
		config = &gorm.Config{}
	}
	db, err := gorm.Open(ds.configuration.Connect(dsn), config)
	if err != nil {
		panic("failed to connect Database")
	}
	ds.log.Debug().
		Msg("DatabaseStarter connection established.")

	return db
}
