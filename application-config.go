package lime

import "gitlab.com/hd/lock"

type ApplicationConfiguration struct {
	models        DatabaseModels
	database      *DatabaseConfiguration
	cors          *CorsConfiguration
	logging       *LogConfiguration
	routerFactory RouterFactory
	port          int
}

func (c *ApplicationConfiguration) Model(model DatabaseModel) *ApplicationConfiguration {
	if c.models != nil {
		c.models = append(c.models, model)
	} else {
		c.models = []DatabaseModel{model}
	}
	return c
}

func (c *ApplicationConfiguration) Models(models DatabaseModels) *ApplicationConfiguration {
	if c.models != nil {
		c.models = append(c.models, models...)
	} else {
		c.models = models
	}
	return c
}

func (c *ApplicationConfiguration) Database(databaseConfig *DatabaseConfiguration) *ApplicationConfiguration {
	c.database = databaseConfig
	return c
}

func (c *ApplicationConfiguration) Logging(logConfiguration *LogConfiguration) *ApplicationConfiguration {
	c.logging = logConfiguration
	return c
}

func (c *ApplicationConfiguration) CorsAllowOrigins(allowOrigins []string) *ApplicationConfiguration {
	if c.cors == nil {
		c.cors = DefaultCorsConfiguration()
	}
	c.cors.AllowOrigins = allowOrigins
	return c
}

func (c *ApplicationConfiguration) CorsAllowMethods(allowMethods []string) *ApplicationConfiguration {
	if c.cors == nil {
		c.cors = DefaultCorsConfiguration()
	}
	c.cors.AllowMethods = allowMethods
	return c
}

func (c *ApplicationConfiguration) CorsAllowHeaders(allowHeaders []string) *ApplicationConfiguration {
	if c.cors == nil {
		c.cors = DefaultCorsConfiguration()
	}
	c.cors.AllowHeaders = allowHeaders
	return c
}

func (c *ApplicationConfiguration) CorsAllowCredentials(allowCredentials bool) *ApplicationConfiguration {
	if c.cors == nil {
		c.cors = DefaultCorsConfiguration()
	}
	c.cors.AllowCredentials = allowCredentials
	return c
}

func (c *ApplicationConfiguration) Router(routerFactory RouterFactory) *ApplicationConfiguration {
	c.routerFactory = routerFactory
	return c
}

func (c *ApplicationConfiguration) Port(port int) *ApplicationConfiguration {
	c.port = port
	return c
}

func (c *ApplicationConfiguration) Configure() *Application {
	runtime := &RuntimeContext{
		Engine:   nil,
		Database: nil,
		Server:   nil,
	}
	app := &Application{
		log:       NewLogger("Application"),
		isRunning: AtomicBool(0),
		stopLock:  lock.New(),
		config:    c,
		runtime:   runtime,
	}
	app.configure()
	return app
}

func ApplicationWith() *ApplicationConfiguration {
	return &ApplicationConfiguration{}
}
