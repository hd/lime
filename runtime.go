package lime

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

type RuntimeContext struct {
	Engine   *gin.Engine
	Server   *http.Server
	Database *gorm.DB
}
