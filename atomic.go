package lime

import "sync/atomic"

type AtomicBool int32

func (b *AtomicBool) isSet() bool { return atomic.LoadInt32((*int32)(b)) != 0 }
func (b *AtomicBool) setTrue()    { atomic.StoreInt32((*int32)(b), 1) }
func (b *AtomicBool) setFalse()   { atomic.StoreInt32((*int32)(b), 0) }
