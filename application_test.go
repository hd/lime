package lime

import (
	"net/http"
	"testing"
	. "time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hd/lock"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func TestApplication_IsRunning(t *testing.T) {
	testContext := CreateTestContext()
	testContext.RunTest(t, func(t *testing.T) {
		app := testContext.application
		Sleep(10 * Millisecond)
		assert.True(t, app.IsRunning())
	})
}

func TestApplication_AcceptsRequest(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Router(NewMockRouter).
		Logging(&LogConfiguration{
			GlobalLevel: InfoLevel,
		}).
		Configure()
	testContext.RunTest(t, func(t *testing.T) {
		for i := 0; i < 20; i++ {
			resp := testContext.GET("/mock")
			assert.NotNil(t, resp)
			assert.Equal(t, http.StatusOK, resp.Code)
			Sleep(10 * Millisecond)
		}
	})
}

func TestApplication_AcceptsRequestOnCustomPort(t *testing.T) {
	router := NewMockRouter
	models := MockDatabaseModels()
	database := NewMockDatabaseConfig()
	testContext := &TestContext{
		application: ApplicationWith().
			Models(models).
			Database(database).
			Router(router).
			Port(9090).
			Configure(),
	}
	testContext.RunTest(t, func(t *testing.T) {
		for i := 0; i < 20; i++ {
			resp := testContext.GET("/mock")
			assert.NotNil(t, resp)
			assert.Equal(t, http.StatusOK, resp.Code)
			Sleep(10 * Millisecond)
		}
	})
}

func TestApplication_AcceptsRequestWithoutLoggingConfiguration(t *testing.T) {
	router := NewMockRouter
	models := MockDatabaseModels()
	database := NewMockDatabaseConfig()
	testContext := &TestContext{
		application: ApplicationWith().
			Models(models).
			Database(database).
			Router(router).
			Configure(),
	}

	testContext.RunTest(t, func(t *testing.T) {
		resp := testContext.GET("/mock")
		assert.NotNil(t, resp)
		assert.Equal(t, http.StatusOK, resp.Code)
	})
}

func TestApplication_StartStop(t *testing.T) {
	app := CreateTestContext().application
	l := lock.New()
	go func() {
		l.Unlock()
		if err := app.Start(); err != nil {
			assert.NoError(t, err)
		}
	}()

	l.Wait()
	app.Stop()
}

func TestApplication_WithCustomDatabaseConfig(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(&DatabaseConfiguration{
			DSN:        "file::memory:?cache=shared",
			Connect:    sqlite.Open,
			GormConfig: &gorm.Config{Logger: logger.Default.LogMode(logger.Info)},
		}).
		Router(NewMockRouter).
		Configure()
	testContext.RunTest(t, func(t *testing.T) {
		for i := 0; i < 20; i++ {
			resp := testContext.GET("/mock")
			assert.NotNil(t, resp)
			assert.Equal(t, http.StatusOK, resp.Code)
			Sleep(10 * Millisecond)
		}
	})
}
func TestApplication_RunWithoutAnyCorsConfig(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Router(NewMockRouter).
		Configure()
	testContext.RunTest(t, func(t *testing.T) {
		for i := 0; i < 20; i++ {
			resp := testContext.GET("/mock")
			assert.NotNil(t, resp)
			assert.Equal(t, http.StatusOK, resp.Code)
			Sleep(10 * Millisecond)
		}
	})
}

func TestApplication_ConfigureWithDefaultCorsConfig(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Router(NewMockRouter).
		Configure()
	assert.NotEmpty(t, testContext.application.config)
	assert.Equal(t, DefaultCorsConfiguration().AllowOrigins, testContext.application.config.cors.AllowOrigins)
	assert.Equal(t, DefaultCorsConfiguration().AllowHeaders, testContext.application.config.cors.AllowHeaders)
	assert.Equal(t, DefaultCorsConfiguration().AllowMethods, testContext.application.config.cors.AllowMethods)
	assert.Equal(t, DefaultCorsConfiguration().AllowCredentials, testContext.application.config.cors.AllowCredentials)
}

func TestApplication_ConfigureWithCorsAllowOrigins(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Router(NewMockRouter).
		CorsAllowOrigins([]string{"https://Test-Origins"}).
		Configure()
	assert.NotEmpty(t, testContext.application.config)
	assert.Equal(t, []string{"https://Test-Origins"}, testContext.application.config.cors.AllowOrigins)
	assert.Equal(t, DefaultCorsConfiguration().AllowHeaders, testContext.application.config.cors.AllowHeaders)
	assert.Equal(t, DefaultCorsConfiguration().AllowMethods, testContext.application.config.cors.AllowMethods)
	assert.Equal(t, DefaultCorsConfiguration().AllowCredentials, testContext.application.config.cors.AllowCredentials)
}

func TestApplication_ConfigureWithCorsAllowMethods(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Router(NewMockRouter).
		CorsAllowMethods([]string{"GET"}).
		Configure()
	assert.NotEmpty(t, testContext.application.config)
	assert.Equal(t, DefaultCorsConfiguration().AllowOrigins, testContext.application.config.cors.AllowOrigins)
	assert.Equal(t, DefaultCorsConfiguration().AllowHeaders, testContext.application.config.cors.AllowHeaders)
	assert.Equal(t, []string{"GET"}, testContext.application.config.cors.AllowMethods)
	assert.Equal(t, DefaultCorsConfiguration().AllowCredentials, testContext.application.config.cors.AllowCredentials)
}

func TestApplication_ConfigureWithCorsAllowCredentials(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Router(NewMockRouter).
		CorsAllowCredentials(true).
		Configure()
	assert.NotEmpty(t, testContext.application.config)
	assert.Equal(t, DefaultCorsConfiguration().AllowOrigins, testContext.application.config.cors.AllowOrigins)
	assert.Equal(t, DefaultCorsConfiguration().AllowHeaders, testContext.application.config.cors.AllowHeaders)
	assert.Equal(t, DefaultCorsConfiguration().AllowMethods, testContext.application.config.cors.AllowMethods)
	assert.True(t, testContext.application.config.cors.AllowCredentials)
}

func TestApplication_ConfigureWithCorsAllowHeaders(t *testing.T) {
	testContext := TestContextWith().
		Models(MockDatabaseModels()).
		Database(NewMockDatabaseConfig()).
		Router(NewMockRouter).
		CorsAllowHeaders([]string{"X-Test"}).
		Configure()
	assert.NotEmpty(t, testContext.application.config)
	assert.Equal(t, DefaultCorsConfiguration().AllowOrigins, testContext.application.config.cors.AllowOrigins)
	assert.Equal(t, []string{"X-Test"}, testContext.application.config.cors.AllowHeaders)
	assert.Equal(t, DefaultCorsConfiguration().AllowMethods, testContext.application.config.cors.AllowMethods)
	assert.Equal(t, DefaultCorsConfiguration().AllowCredentials, testContext.application.config.cors.AllowCredentials)
}
