package lime

import (
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"testing"
)

func TestDatabaseStarter_Start(t *testing.T) {
	starter := &DatabaseStarter{
		log:           NewLogger("DatabaseStarter"),
		configuration: NewMockDatabaseConfig(),
	}
	db := starter.Start()
	assert.NotEmpty(t, db)
}

func TestDatabaseStarter_Start_Error(t *testing.T) {
	starter := &DatabaseStarter{
		log: NewLogger("DatabaseStarter"),
		models: []DatabaseModel{
			&Mock{},
		},
		configuration: &DatabaseConfiguration{
			DSN:     "",
			Connect: func(string) gorm.Dialector { return nil },
		},
	}
	AssertPanic(t, func() {
		starter.Start()
	})
}
