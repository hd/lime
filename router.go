package lime

import "github.com/gin-gonic/gin"

type RoutedController interface {
	Register(engine *gin.Engine)
}

type Router struct {
	Routes []RoutedController
}

func (r *Router) RegisterRoutes(engine *gin.Engine) {
	for _, route := range r.Routes {
		route.Register(engine)
	}
}

type RouterFactory func(runtime *RuntimeContext) *Router
